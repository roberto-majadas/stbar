# Stbar

Stbar is a versatile command-line tool inspired by the Spanish word "estibar" meaning "to stack" or "to arrange in order." This project aims to streamline the process of building, maintaining, and publishing multiple containers, empowering users to efficiently manage containerized environments.

With stbar, users can seamlessly generate configurations for GitLab CI pipelines, simplifying the integration of containers into continuous integration workflows. Additionally, stbar provides functionality for building containers, even when they are interrelated, ensuring smooth deployment and management of complex containerized systems.

## Requirements

To use Stbar, you'll need the following requirements installed on your system:

- Python
- Podman

## Installation

You can easily install Stbar using `pip`:

```bash
pip install stbar
```

### Developer Installation

If you're a developer and want to contribute to Stbar or use the latest development version, follow these steps:

1. Clone the Git repository:

   ```bash
   git clone https://gitlab.com/roberto-majadas/stbar
   ```

2. Create a virtual environment (optional but recommended):

   ```bash
   python -m venv venv
   ```

3. Activate the virtual environment:

   - On macOS and Linux:

     ```bash
     source venv/bin/activate
     ```

   - On Windows:

     ```bash
     .\venv\Scripts\activate
     ```

4. Install Stbar in development mode:

   ```bash
   pip install -r requirements/dev.txt
   pip install -e .
   ```

### Testing Requirements

If you want to enable testing requirements, use the following command:

```bash
pip install -r requirements/dev.txt
```

To run the tests, execute:

```bash
tox -e py
```

### Linters

To run the linters, execute:

```bash
tox -e lint
```

## Usage

Once Stbar is installed, you can run it in your terminal by using the following command:

```bash
stbar -h
```

## Contribution

If you're interested in contributing to Stbar, please read the [CONTRIBUTING.md](CONTRIBUTING.md) file in this repository.

## License

Stbar is released under the MIT License. See the [LICENSE](LICENSE) file for more details.


## Authors

- Roberto Majadas
