import tempfile
from contextlib import chdir
from pathlib import Path

import pytest

from stbar.settings import Settings


@pytest.fixture
def tmpdir():
    with tempfile.TemporaryDirectory() as tmpdir:
        with chdir(tmpdir):
            yield tmpdir


def test_settings(tmpdir):
    Path(tmpdir, ".stbar.toml").touch()
    settings = Settings()
    assert settings.project.root == tmpdir


def test_settings_without_config_file(tmpdir):
    settings = Settings()
    assert settings.project.root == ""
