import tempfile
from contextlib import chdir
from pathlib import Path

import pytest
from dynaconf.base import Settings

from stbar.utils import dump_settings, find_project_settings


def test_find_project_settings():
    # Test the case when the settings file is in the current directory
    with tempfile.TemporaryDirectory() as tmpdir:
        config_file = Path(tmpdir, ".stbar.toml")
        config_file.touch()
        assert find_project_settings(Path(tmpdir)) == config_file


def test_find_project_settings_in_a_parent_dir():
    with tempfile.TemporaryDirectory() as tmpdir:
        config_file = Path(tmpdir, ".stbar.toml")
        config_file.touch()
        nested_dir = Path(tmpdir, "test")
        nested_dir.mkdir()
        with chdir(nested_dir):
            assert find_project_settings(Path(tmpdir)) == config_file


def test_find_project_settings_with_invalid_path():
    assert find_project_settings(Path("invalid/path")) is None


def test_find_project_settings_without_settings():
    with tempfile.TemporaryDirectory() as tmpdir:
        assert find_project_settings(Path(tmpdir)) is None


def test_find_project_settings_with_no_argument():
    with tempfile.TemporaryDirectory() as tmpdir:
        config_file = Path(tmpdir, ".stbar.toml")
        config_file.touch()
        with chdir(tmpdir):
            assert find_project_settings() == config_file


def test_dump_settings():
    settings = Settings()
    assert dump_settings(settings) == ""

    # Test the case when the output format is 'toml'
    settings = Settings()
    settings.set("root", "/root")
    assert dump_settings(settings, output_format="toml") == 'root = "/root"\n'

    # Test the case when the output format is 'json'
    settings = Settings()
    settings.set("root", "/root")
    assert dump_settings(settings, output_format="json") == '{"root": "/root"}'

    # Test the case when the output format is 'yaml'
    settings = Settings()
    settings.set("root", "/root")
    assert dump_settings(settings, output_format="yaml") == "---\nroot: /root\n"


def test_dump_settings_invalid_format():
    settings = Settings()
    with pytest.raises(ValueError, match="Invalid output format"):
        dump_settings(settings, output_format="invalid")
