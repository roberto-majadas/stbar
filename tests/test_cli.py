import importlib
import sys
from collections import namedtuple
from pathlib import Path

import pytest
from click.testing import CliRunner, Result

from stbar import __version__


@pytest.fixture
def env():
    runner: CliRunner = CliRunner()
    with runner.isolated_filesystem() as tmpdir:

        def invoke_cli(args):
            cli_mod = importlib.import_module("stbar.cli")
            result = runner.invoke(cli_mod.cli, args)
            for mod in list(sys.modules.keys()):
                if mod.startswith("stbar"):
                    del sys.modules[mod]
            return result

        env = namedtuple("Env", ["cli", "tmpdir"])
        yield env(cli=lambda args: invoke_cli(args), tmpdir=tmpdir)


def test_show_cli_help(env):
    result: Result = env.cli(["--help"])
    assert (
        "version" in result.output.strip()
    ), "Version string should should appear in the command list."


def test_version_displays_library_version(env):
    result: Result = env.cli(["version"])
    assert (
        __version__ in result.output.strip()
    ), "Version number should match library version."


def test_init_cmd(env):
    result: Result = env.cli(["init"])
    assert result.exit_code == 0, "Init command should exit with code 0."
    assert Path(
        env.tmpdir, ".stbar.toml"
    ).exists(), "Init command should create .stbar.toml file."


def test_init_cmd_with_existing_config(env):
    Path(env.tmpdir, ".stbar.toml").touch()
    result: Result = env.cli(["init"])
    assert result.exit_code != 0, (
        "Init command should exit with non-zero code " "if .stbar.toml already exists."
    )


def test_config_show_cmd(env):
    Path(env.tmpdir, ".stbar.toml").touch()
    result: Result = env.cli(["config", "show"])
    assert result.exit_code == 0
    assert result.output == f'[project]\nroot = "{env.tmpdir}"\n\n'


def test_config_show_cmd_without_config_file(env):
    result: Result = env.cli(["config", "show"])
    assert result.exit_code == 1
