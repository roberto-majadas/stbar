import os

__version__ = "0.0.1" + os.getenv("STBAR_GIT_RELEASE_VERSION", default="")
