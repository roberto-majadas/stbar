import tempfile
from pathlib import Path
from typing import Optional

from dynaconf import loaders
from dynaconf.base import Settings


def find_project_settings(path: Optional[Path] = None) -> Optional[Path]:
    if path is None:
        current_path = Path.cwd()
    else:
        current_path = path
    project_settings_path = current_path.joinpath(".stbar.toml")

    if project_settings_path.exists() and project_settings_path.is_file():
        return project_settings_path

    if current_path.parent == current_path:
        return None

    return find_project_settings(current_path.parent)


def dump_settings(settings: Settings, output_format: str = "toml") -> str:
    with tempfile.TemporaryDirectory() as tmpdir:
        exported_file = tmpdir + "/export"
        data = {key.lower(): val for key, val in settings.as_dict().items()}

        if output_format == "toml":
            loaders.toml_loader.write(exported_file, data)
        elif output_format == "json":
            loaders.json_loader.write(exported_file, data)
        elif output_format == "yaml":
            loaders.yaml_loader.write(exported_file, data)
        else:
            raise ValueError("Invalid output format")

        with open(exported_file) as fd:
            return fd.read()
