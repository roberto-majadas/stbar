import sys
from pathlib import Path

from dynaconf import Dynaconf

from stbar.utils import find_project_settings

DEFAULT_SETTINGS = Path(sys.prefix).joinpath("share", "stbar", "conf", "default.toml")


class Settings(Dynaconf):  # type: ignore[misc]
    def __init__(self):
        preload_settings = [DEFAULT_SETTINGS]
        project_settings_path = find_project_settings(Path.cwd())
        if project_settings_path is not None:
            preload_settings.append(project_settings_path)

        super().__init__(
            environments=False, envvar_prefix="STBAR", preload=preload_settings
        )

        if project_settings_path is not None:
            self.project.root = project_settings_path.parent.as_posix()

        self.unset("GIT_RELEASE_VERSION")


settings = Settings()
