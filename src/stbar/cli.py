import sys
from pathlib import Path

import click

from stbar import __version__
from stbar.settings import settings
from stbar.utils import dump_settings


@click.group()
def cli():
    pass


@cli.command()
@click.option(
    "-c",
    "--config",
    default=Path(sys.prefix).joinpath("share", "stbar", "conf", "init.toml"),
    type=click.File(mode="r"),
    help="Init configuration file",
)
def init(config):
    """Initialize the stbar environment"""
    if Path(".stbar.toml").exists():
        click.echo("The .stbar.toml file already exists")
        exit(1)
    else:
        with open(".stbar.toml", "w") as fd:
            fd.write(config.read())


@cli.group()
def config():
    pass


@config.command(name="show")
@click.option(
    "--output-format",
    default="toml",
    help="Output format",
    type=click.Choice(["toml", "json", "yaml"]),
)
def config_show(output_format):
    """
    Show the configuration settings
    """
    if settings.project.root == "":
        click.echo("No .stbar.toml file detected. Please execute stbar init first.")
        exit(1)
    print(dump_settings(settings, output_format=output_format))


@cli.command()
def version():
    """Get the stbar version."""
    click.echo(click.style(f"{__version__}", bold=True))
